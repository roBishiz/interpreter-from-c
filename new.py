import sys
import re

input_ = open(sys.argv[1], "r")
output = open(sys.argv[2], "w")

# testLine = input_.readlines()[0]
# if (testLine[6] == 'a') ^ \
#         (testLine[4] == 'a'):
#     print(0)

# if re.match(r'^/\*$', '/*'):
#    print('yes')
# else:
#    print('no')

# print("This is the name of the script: ", sys.argv[0])
# print("Number of arguments: ", len(sys.argv))
# print("The arguments are: ", str(sys.argv))

# re.compile documentation:
#
# lib re definition
# \d is any num like [0-9]
# \b is word boundary
# \w is any num or letter like [a-zA-Z0-9_]

lexems = {'^0[bB][01]+$': 'Int\tint1:',  # Binary
          '^0[0-8]*$': 'Int\tint2:',  # Octal
          '^[1-9][\d]*$': 'Int\tint3:',  # Decimal
          '^0[xX][\dABCDEFabcdef]+$': 'Int\tint4:',  # Hexadecimal
          '^write$': 'Write\tval:',
          # r',': 'Comma\tval:',
          # r';': 'Semicolon\tval:',
          '^[a-zA-Z_][\w_]*$': 'Id\tval:',
          # r'^[eE][-+]?\d+$': 'Real\treal:',  # Порядок [eE][-+]?\d+ числовая строка \d+
          # r'/\*': 'Comment\tval:',# /\*
          # r'\*/': 'Comment\tval:',# \*/
          '^\d+.\d*([Ee][+-]?\d+)?$|^\d+[Ee][+-]?\d+$|^.\d+([Ee][+-]?\d+)?$': 'Real\treal:'}  # Error always need to be in end

words = [
    'Var', 'Vector', 'Goto',
    'Read', 'Write', 'Break',
    'Tools', 'Proc', 'Call',
    'If', 'Case', 'Then',
    'Else', 'Of', 'Or',
    'While', 'Loop', 'Do'
]

# ':': 'Label\tval',

for word in words:
    lexems['^' + word.lower() + '$'] = word + '\tval:'

lexems[list(lexems.keys())[5][:-1:] + ':&'] = 'label\tval'

symbols = [
    '\(', '\)', '\[', '\]', '\{', '\}', '-', '\+', '\*', '%', '/', '=', '>', '<', '!=', '==', '<=', '>=', '/\*', ',',
    ';', '\*/'
]

designations = [
    'LRB', 'RRB', 'LSB', 'RSB', 'LCB', 'RCB', 'Min', 'Add', 'Mul', 'Mod', 'Div', 'Let', 'GT', 'LT', 'NE', 'EQ', 'LE',
    'GE', 'Comment', 'Comma', 'Semicolon', 'Comment'
]

startComment = '^/\*$'
endComment = '^\*/$'

for i in range(len(symbols)):
    lexems['^' + symbols[i] + '$'] = designations[i] + '\tval:'

originalSplitLetters = ['!=',
                        '==',
                        '<=',
                        '>=',
                        '\(',
                        '\)',
                        '\[',
                        '\]',
                        '\{',
                        '\}',
                        '-',
                        '\+',
                        '\*',
                        '%',
                        '/',
                        '=',
                        '>',
                        '<',
                        '/\*',
                        ',',
                        ';',
                        '\*/']

finalSplitLetters = [' != ',
                     ' == ',
                     ' <= ',
                     ' >= ',
                     ' ( ',
                     ' ) ',
                     ' [ ',
                     ' ] ',
                     ' { ',
                     ' } ',
                     ' - ',
                     ' + ',
                     ' * ',
                     ' % ',
                     ' / ',
                     ' = ',
                     ' > ',
                     ' < ',
                     ' /* ',
                     ' , ',
                     ' ; ',
                     ' */ ']

lexems[r'\b\w*\b'] = 'Error\tval'

# [\n,\x20,\0]
# Двоичное число
# re.findall(r'\b0[bB][01]+\b', '0B10101')
# Восьмеричное число
# re.findall(r'\b0[0-8]*\b', '01')
# Десятиричное число
# re.findall(r'\b[1-9]\d*\b', '112341999')
# Шестнадцатеричное число
# re.findall(r'\b0[xX][\dABCDEFabcdef]+\b', '0xf12afaf')
# Распарсивание файла

iteratorLine = 1
flag = True
flagComment = True  # true if we not in start of comment
iteratorComment = 0
for line in input_.readlines():
    line = ' ' + line + ' '
    for i in range(len(originalSplitLetters)):
        originalLetter = originalSplitLetters[i]
        finaliLetter = finalSplitLetters[i]
        # TODO: Сделать вызов Error, если справа от < или > знак равно =
        if originalLetter in ['!=', '==', '<=', '>=']:
            line = re.sub(originalLetter, '#' + str(i), line)
        else:
            line = re.sub(originalLetter, finaliLetter, line)
    for i in range(len(originalSplitLetters)):
        line = re.sub('#' + str(i), finalSplitLetters[i], line)
    line = line.split()
    for unit in line:
        for pattern in lexems:
            if re.match(pattern, unit) and (flagComment or pattern == endComment):
                # print(lexems[pattern] + ': ' + pattern + ' in ' + unit)
                output.write(str(iteratorLine) + '\tlex:' + lexems[pattern] + unit + '\n')
                if pattern == startComment:
                    iteratorComment += 1
                    flagComment = False
                elif pattern == endComment:
                    iteratorComment -= 1
                    flagComment = True
                if lexems[pattern] == 'Error\tval' or iteratorComment < 0:  # lexeme error
                    flag = False
                    print('Error:' + unit)
                break
    iteratorLine += 1
if flag:
    print('OK')